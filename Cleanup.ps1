﻿#Cleanup Script für Schnupperlehrlinge
$UserDir = "C:\Users\Schnupperlehrling"

Add-Type -AssemblyName System.Windows.Forms

$ErrorActionPreference = "silentlyContinue"

#Vorlage in einem Versteckten und schreibgeschützen Ordner ablegen
$Vorlage = "C:\Users\Schnupperlehrling\Vorlage_hidden"

#Ordner, welche erhalten werden sollen
$Folders = "Desktop", "Downloads", "Documents"

$answer = [System.Windows.Forms.MessageBox]::Show("Alle Dateien im User Ordner werden gelöscht. Fortfahren?", "Cleanup Scritp", YesNo)
if ($answer -eq [System.Windows.Forms.DialogResult]::No){
    write-host "Script abgebrochen"
    exit}

$Removal = Get-ChildItem -Recurse -Force -Path $UserDir -Exclude "$UserDir\AppData\*"
$Removal.ForEach({ 
    $Removal = $Removal| Where-Object { $_.FullName -notlike "*Vorlage_hidden*" -and $_.FullName -notlike "*.vscode*" -and $_.FullName -notlike "*AppData*" -and $_.Name -notin $Folders -and $_.Name -notlike "Cleanup.ps1" }
    write-output ("Wird gelöscht:" + $_.FullName)
})

$Removal | ForEach-Object{
    Remove-Item $_.FullName -Force -Recurse
}

attrib.exe -h $Vorlage

Copy-Item $Vorlage -Destination "$UserDir\Documents\Vorlagen" -Recurse
Copy-Item $Vorlage -Destination "$UserDir\Desktop\Vorlagen" -Recurse

attrib.exe +h $Vorlage

#Erstellt von Marvin Klingler