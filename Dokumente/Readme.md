# Infoblatt für Betreuer

- Die Aufträge und Dokumente sind alle hier in Github abgelegt. Auf den Schnupper PCs muss GIT installiert sein, damit diese die Dateien benutzen können.

- Als Einführung wird eine kurze Vorstellungsrunde gehalten und danach die Präsetation durchgeführt. Diese ist im `Dokumente` Ordner abgelegt. Zum Schluss der Präsentation wird noch ein Video gezeigt, welches du unter <https://www.youtube.com/watch?v=gfIUsx_WrY8> findest.

- Das Passwort für die Schnuppi-PCs ist `Schnupperlehre`

- Die Schnuppi-PCs sollen mit dem Netzwerk `AveniqGuest` verbunden werden. Melde die Schnuppi-PCs über die Anmelde-Möglichkeit `Login mit Mobiltelefon` an. Gib dafür deine Nummer und anschliessend den erhaltenen Zugangscode ein.

- Die Schnupperlehrlinge müssen früh genug gefragt werden, ob sie ein Bewertungsblatt dabei haben. Falls sie noch eines brauchen, muss noch eines ausgedruckt werden. Dieses kann unter `Dokumente\Schnupperbewertung_Aveniq.docx` gefunden werden.

- Beim Auftrag `Computer Hardware` kannst du die Komponenten des Desktop PCs zeigen. Wenn man eine VM zur Verfügung hat und diese den Schnuppis noch zeigen möchte, kann man das sehr gerne machen.

- Am Nachmittag musst du Matthias Hippen informieren, wenn die Schnuppis die Schnuppertagebücher fertig ausgefüllt haben, damit er die Schnuppis für das Abschlussgespräch holt.

- Die Schnuppertagebücher müssen auf dem USB-Stick gespeichert werden. *Bitte die alten Tagebücher auf dem Stick nicht löschen.*

- Anmeldedaten Office und Visual Studio für Schnuppi-PC: `schnupperlehrling_gia_2019@outlook.com` und `Passwordw2k`.

- Am Ende des Tages wird eine Broschüre abgegeben. Sämtliche Dateien dazu sind im Unterordner `Abgaben`. Drucke alle Dateien aus. Eine Vorlage für die Fertige Broschüre solltest du bereits erhalten haben. Frage sonst noch danach.

- Wenn neue Ordner erstellt werden, muss darauf geachtet werden, dass diese keine Sonderzeichen enthalten. Ansonsten kann es zu Problemen bei den Links im Gitlab kommen.
