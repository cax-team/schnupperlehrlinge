# Aufträge                                                  


## [1-Künstliche Intelligenz](./1-Kuenstliche_Intelligenz_AI/README.md)

In diesem Auftrag geht es darum, mit AI Tools zu experimentieren und zu lernen, inwiefern diese nützlich sein können.

## [2-Excel Formeln anwenden](./2-Excel_Formeln_anwenden/Readme.md)

Hier lernst du eine der wichtigsten Funktion von Excel kennen.

## [3-Webseite mit HTML erstellen](./3-Webseite_mit_HTML/)

Du lernst die Grundlagen kennen, wie eine Webseite aufgebaut wird und wie diese funktioniert.

## [4-Snake mit Python](./4-Snake_mit_Python/Readme.md)

Programmiere hier ein simples Spiel mit Python und der PyGame Library.

## [5-Rechner in C# programmieren](./5-Rechner_in_C_sharp_programmieren/Readme.md)

Mit C# und Windows Forms wirst du hier ein modern aussehendes Programm erstellen.

## [6-PowerShell Script](./6-PowerShell_Skript/Readme.md)

Erstelle ein kurzes PowerShell Script um die Grundlagen für Scripting kennen zu lernen.

## [7-Würfel in C# programmieren](./7-Wuerfel_in_C_sharp_programmieren/Readme.md)

Als Zusatzaufgabe kannst du hier dein Verständniss von C# unter beweis setzen.

## [8-Python Webrequest](./8-Python_Webrequest/Readme.md)

Als Zusatzaufgabe kannst du hier dein Verständnis von Python unter beweis setzen.

## [9-Pong in Python](./9-Pong_in_Python/README.md)

In diesem Auftrag programmierst du das klassische Pong-Spiel mit Python und Pygame. Dabei werden sowohl das linke Paddle als auch das rechte Paddle vom Computer gesteuert.

## [10-Webseite mit vue](./10-Webseite_mit_Vuejs/README.md)

In diesem Auftrag erstellst du eine interaktive Webseite mit Vue.js. Dabei sollst du lernen, wie man Funktionen benutzt und wie man die mit Buttons Verknüpfst.

## [11-Grundlagen im Linux Terminal](./11-Grundlagen_im_Linux_Terminal/Readme.md)

In diesem Auftrag lernst du die Grundlagen vom Terminal in Linux