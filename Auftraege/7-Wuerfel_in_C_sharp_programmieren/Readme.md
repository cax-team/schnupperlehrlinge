# Würfel in C\#

## 1 Programmentwicklung

### 1.1 Erstellen der Benutzeroberfläche

Als erstes startest du wieder Visual Studio 2022. Erstelle ein neues Projekt und wähle wie bereits beim Rechner eine Windows Forms-App (.NET Framework). Dem Projekt kannst du z.B. den Namen Würfel oder ähnlich geben. Wähle *Durchsuchen* um unseren Ordner Vorlagen als Speicherort anzugeben. Nach einem Klick auf OK erscheint folgendes Fenster:

![Leeres Forms](./Bilder/Leeres%20Form.png)

Öffne das Programm Microsoft Paint. Zeichne einen 300 x 300 Pixel grossen Hintergrund für den Würfel. Den Hintergrund färbst du in einer beliebigen Farbe ein und speicherst ihn im Ordner *Vorlagen* mit dem Dateinamen `hintergrund`. Nun machen wir die Augen. Stelle die Grösse auf 40 x 40 Pixel ein und nehmen die gleiche Hintergrundfarbe wie vorhin. Auf den Hintergrund zeichnest du nun einen ausgefüllten Kreis und speicherst das Ganze unter dem Namen `auge` im Ordner Vorlagen ab.
Nun musst du diese Sachen in dein Programm einbauen. Als erstes machst du eine neue PictureBox auf dem Formular und wählst im Eigenschaften-Fenster Image. Wenn du auf den kleinen Knopf hinten geklickt hast, erscheint ein Importfeld. Hier wählst du Projektressourcendatei: und klickst auf *Importieren* und wählst den Hintergrund, welchen du vorhin gezeichnet hast.

![Import](./Bilder/Import.png)

Nun musst du noch die Grösse der PictureBox auf die Grösse des Hintergrundes abstimmen. Als nächstes erstellst du eine neue PictureBox und wählst als Image das Bild des Auges. Du kannst noch die Grösse anpassen damit kein weisser Rand übrig bleibt und dann kopierst du das Element 6-mal damit du sieben Augen hast. Das Ganze sollte nun etwa so aussehen.

![Fertiger Wuerfel](./Bilder/Fertiger%20Wuerfel.png)

Nun gibst du den Augen einen Namen. Den Namen kannst du im Eigenschaften Fenster unter `Name` finden. Du überlegst dir eine gute Benennung, damit du wisst welches Auge wo ist. Auf der letzten Seite ist ein Beispiel für die Beschriftung.
Nun stellst du noch den Namen, der Form ein, damit nicht Form1 im Titel steht. Das kannst du im Eigenschaftsfenster der Form unter `Text` ändern. Des Weiteren möchtest du, dass die Grösse verändert wird, deshalb kannst du die Eigenschaft `FormBorderStyle` auf `FixedDialog`.
Jetzt fehlt noch ein Knopf zum Würfeln. Den kannst du ganz einfach mit dem Werkzeug Button unter dem Würfel hineinziehen. Die Eigenschaft `Text` definiert den angezeigten Text, hier gibst du nun Würfeln ein. Des Weiteren gibst du dem Button den Namen `cmdWuerfeln`.
Was bei keinem professionellen Programm fehlen darf, ist die Menüleiste. Um sie zu erstellen, klickst du in der Toolbox auf `MenuStrip`. Hier kannst du nun Menü Einträge gestalten. Gib nun `&Datei` in das erste Feld ein. Das *&* sorgt dafür das man das Feld über Tastenkombinationen ansprechen kann. Das *D* wird unterstrichen dargestellt. Im Datei Menü gibst du nun `&Über` und `&Beenden` ein. Das sieht dann so aus:

![Menu Strip](./Bilder/Menu%20Strip.png)

Der erste Teil wäre nun geschafft. Du kannst den Würfel schon mal begutachten, indem du auf das Playsymbol (grüner Pfeil) klickst oder F5 drückst.

### 1.2 Code schreiben

#### 1.2.1 Menüfunktionen

Nun willst du, dass der Würfel auch noch etwas tut. Dafür musst du den Code programmieren. Du solltest mit dem Beenden beginnen, weil es das Einfachste ist. Klicke erst mal im Menu auf Datei und dann machst du einen Doppelklick auf Beenden. Sofort öffnet sich ein neues Fenster:

```C#
private void beendenToolStripMenuItem_Click(object sender, EventArgs e){

}
```

Hier gibst du nun `this.Close()`; ein. Wenn du den Würfel jetzt testest, kannst du ihn über das Beenden der Menüleiste schliessen.
Nun programmierest du noch das Über, dazu machst du einen Doppelklick auf `Über`, nun sollte sich wieder das Fenster öffnen. Du gibst jetzt jedoch nicht `this.Close()`; ein sondern:

```C#
 MessageBox.Show ("Dieser virtuelle Würfel wurde von\r\n(dein Name hier) gestaltet und programmiert! ", "Über den Würfel", MessageBoxButtons.OK, MessageBoxIcon.Information)
```

Erklärung:
```MessageBox.Show("Text", "Titel", style)``` Das `\r\n` sorgt für den Zeilenumbruch.

![Message Box](./Bilder/Message%20Box.png)

Wenn du das Programm testest, wird ein solches Feld erscheinen, wenn du auf Über klickst.

#### 1.2.2 Würfelfunktion

Und jetzt zum komplizierteren, aber wichtigen Teil des Würfels, dem Würfeln. Um den Code eingeben zu können, musst du den Button Würfeln doppelklicken.
Als erstes erzeugst du eine Zufallszahl:

```C#
  int Zufallszahl = 0;
            Random Zahl = new Random();
            Zufallszahl =Zahl.Next(1, 7);
```

Mit diesem Befehl wird eine Zufallszahl zwischen 1 und 6 generiert, die dann in der Variabel int Zufallszahl gespeichert wird.
Nun machst du alle Augen erst mal unsichtbar:

```C#
imgAuge1.Visible = false;
imgAuge2.Visible = false;
imgAuge3.Visible = false;
usw…
```

Jetzt zeigst du die einzelnen Augen wieder an.

```C#
switch (Zufallszahl)
    {
    case 1:
        imgAuge4.Visible = true;
        break;
    case 2:
        imgAuge3.Visible = true;
        imgAuge5.Visible = true;
        break;
    case 3:
        imgAuge3.Visible = true;
        usw…
}
```

Die switch case Anweisung überprüft die Variabel Zufallszahl und springt dann zu dem Punkt, wo Case denselben Wert hat. Hat die Variabel zum Beispiel den Wert 3 erhalten, so wird der Code nach case 3 ausgeführt. Das heisst, der case 4 heisst, dass du eine 4 gewürfelt hast. Du musst nun die vier äusseren Augen sichtbar machen. Du kannst den obigen Text nun bis zu case 6 zu Ende schreiben und somit die jeweiligen Augen sichtbar machen. Danach kannst du den Würfel wieder testen. Nun sollte der Würfel funktionieren.

## 2 Lösung

### 2.1 Code der Lösung

Der fertige Code sollte etwa so aussehen:
für den **Würfeln** Button:

```C#
private void cmdWuerfeln_Click(object sender, EventArgs e){

    int Zufallszahl = 0;
    Random Zahl = new Random();
    Zufallszahl =Zahl.Next(1, 7);

    imgAuge1.Visible = false;
    imgAuge2.Visible = false;
    imgAuge3.Visible = false;
    imgAuge4.Visible = false;
    imgAuge5.Visible = false;
    imgAuge6.Visible = false;
    imgAuge7.Visible = false;

    switch (Zufallszahl)
    {
        case 1:
            imgAuge4.Visible = true;
                break;
        case 2:
            imgAuge3.Visible = true;
            imgAuge5.Visible = true;
            break;
        case 3:
            imgAuge3.Visible = true;
            imgAuge4.Visible = true;
            imgAuge5.Visible = true;
            break;
        case 4:
            imgAuge1.Visible = true;
            imgAuge3.Visible = true;
            imgAuge5.Visible = true;
            imgAuge7.Visible = true;
            break;
        case 5:
            imgAuge1.Visible = true;
            imgAuge3.Visible = true;
            imgAuge4.Visible = true;
            imgAuge5.Visible = true;
            imgAuge7.Visible = true;
            break;
        case 6:
            imgAuge1.Visible = true;
            imgAuge2.Visible = true;
            imgAuge3.Visible = true;
            imgAuge5.Visible = true;
            imgAuge6.Visible = true;
            imgAuge7.Visible = true;
            break;
    }
}
```

für den **Exit** Menüeintrag:

```C#
private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
         {
             this.Close();
}
```

für den **Über** Menüeintrag:

```C#
private void überToolStripMenuItem_Click(object sender, EventArgs e)
         {
MessageBox.Show("Dieser virtuelle Würfel wurde von\r\nName gestaltet und programmiert!","Über den Würfel", MessageBoxButtons.OK, MessageBoxIcon.Information);
}
```

### 2.2 Namensgebung

![Beschrifteter Wuerfel]()
