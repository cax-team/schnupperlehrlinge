# IP-Adresse pingen

## 1 Aufgabenstellung

In dieser Aufgabe wirst du einen Partner anpingen. Du wirst lernen was eine IP-Adresse ist und was die ping Funktion ist.<br>
**Wichtig! Dies ist keine Einzelarbeit. Arbeite mindestens zu zweit.**

## 2 Einführung

### 2.1 Was ist eine IP-Adresse ? 

Eine IP-Adresse ist wie eine Adresse für deinen Computer im Internet. Sie hilft, deinen Computer von anderen zu unterscheiden und ermöglicht es Websites, dir Inhalte zu liefern. 

### 2.2 Was ist der ping-Befehl

Der ping-Befehl ist ein Netzwerk-Tool, das überprüft, ob ein Computer erreichbar ist, und die Reaktionszeit misst. Er sendet ICMP-Pakete (Internet Control Message Protocol) an das Ziel und zeigt an, ob eine Antwort zurückkommt. So weiss man ob man eine Verbindung zum anderen Computer verbinden kann.

## 3 Aufgabe

### 3.1 IP-Adresse herausfinden

1.	Drücke `Windowstaste + R`

2.	In dem Menü das auftaucht gibst du `CMD` und danach `Enter` ein:

    ![WinR](./Bilder/WinR.jpg)

3.	Daraufhin kommst du in die «Eingabeaufforderung». Dort gibst du nun `ipconfig` ein und danach `Enter`

5.	Nachdem du diesen Command eingegeben hast sollte nun eine Art von Tabelle auftauchen mit verschiedenen Adressen. Dort suchst du dir jetzt deine IPv4-Adresse vom WLAN-Adapter raus:

    ![ipconfig](./Bilder/ipconfig.png)

### 3.2 pingen

1. Teile deine IP-Adresse deinem Partner mit

2. Pinge deinen Partner an indem du in die Eingabeaufforderung `ping IP-Adresse` eingibst
    Zum Beispiel: `ping 10.18.130.26`

3. Wenn in der Klammer «0% Verlust» steht, konntest du erfolgreich deinen Partner anpingen