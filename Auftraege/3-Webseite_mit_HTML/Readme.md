# HTML Webseite erstellen

## 1 Der Aufbau einer HTML - Seite kurz erklärt

### 1.1 HTML-Dokument erstellen

Öffne `Visual Studio Code`.

![VScode](./Bilder/VSCode.png)

Um ein Dokument zu erstellen, klickst du oben links auf den Reiter `File` und dann auf `New Text File`.

![Datei erstellen](./Bilder/Neue%20datei%20erstellen.png)

Im Fenster, das sich geöffnet hat, kannst du auf `Select a language` klicken. Im geöffneten Suchfeld gibst du `HTML` ein und klickst Enter. Du hast nun deine Datei für die Webseite erstellt. Damit deine Arbeit nicht verloren geht, solltest du die HTML-Datei speichern. Dafür gehst du wieder auf `File` und klickst dann auf `Save`. Speichere die Datei in deinem Ordner `Vorlagen` auf dem Desktop.

![Abspeichern](./Bilder/Datei%20abspeichern.png)

### 1.2 Tags

Eine HTML (Hyper Text Markup Language) Seite besteht aus sogenannten `Tags`. Diese bestehen aus einer einleitenden eckigen Klammer `<`, einem Namen (z.B. title) und einer schliessenden eckigen Klammer `>`. Zusammengesetzt ergibt das `<title>`.

Im öffnenden Tag können noch zusätzliche Angaben stehen bei `<hr>` (Horizontale Linie) beispielsweise die Höhe: `<hr height="5">`.
Mit wenig Ausnahmen gehört zu jedem öffnenden Tag auch ein schliessendes End-Tag. Dieses setzt sich aus der öffnenden Klammer, einem Slash `/`, dem Namen des einleitenden Tags und der schliessenden Klammer zusammen. Eine der wenigen Ausnahmen ist `<hr>` (Horizontale Linie).
Das Ganze sieht dann so aus: `</title>`

In HTML müssen einige Tags zwar nicht explizit geschlossen werden, jedoch gehört dies zum guten Stil, und es kann je nach Browser zu unerwünschten Nebeneffekten führen, wenn man diese offen lässt.

Tags können (und müssen beim normalen Gebrauch auch) verschachtelt werden. Also z.B. `<tag1><tag2>Irgendwas</tag2></tag1>`. Dabei ist zu beachten, dass der zuletzt geöffnete tag als erster wieder geschlossen werden muss.

## 1.3 Die drei wichtigsten Tags

### 1.3.1 `<html></html>`

Definiert den Beginn und das Ende einer HTML-Seite. Vor und hinter diesen Tags darf nichts untergebracht werden.

### 1.3.2 `<head></head>`

Im Kopfbereich werden der Titel, Informationen für den Browser oder Suchmaschinen, sowie z.B. JavaScript-Code abgelegt. Der Inhalt des Kopfbereichs wird also nicht im Anzeigefenster des Browsers wiedergegeben.

### 1.3.3 `<body></body>`

 Im `Body` wird alles hinterlegt, dass der Browser darstellen soll. Also den Inhalt der Webseite.

```html
<html>
        <head>
                    <title>Testseite</title>
        </head>
        <body>
                    Weiterer Code für die Website
        </body>
</html>
```

## 2 Planung

### 2.1 Vorüberlegungen

Bevor man sich vor den Computer setzt, Visual Studio Code startet und einfach mal `Hallo. Willkommen auf meiner Homepage!` schreibt, sollte man sich etwas Zeit nehmen, um seine Seite zu planen.
Dazu sollte man sich folgendes überlegen:

#### 2.1.1 Wie soll das ganze einmal aussehen

##### 2.1.1.1 So sicher nicht

Hier ein Auszug, was man sicher nicht wählen sollte:

- Schwarzer Hintergrund
- Drehende E-Mail – Logos
- Möglichst viele Werbebanner
- Einige Pop-Up-Windows
- Dunkelgrauer Text
- Bilder wie sich der Internet Explorer und der Mozilla Firefox gegenseitig die Köpfe einschlagen.

##### 2.1.1.2 Schon eher

- Kleines Logo der Homepage
- Statische Navigation
- Für das Auge angenehme Farben

## 3 Grafiken / Bilder

Einige Stichworte:

- Sollten möglichst klein (Dateigrösse) gehalten werden
- Eigene sehen nicht nur besser aus als geklaute, man kann auch mit Stolz sagen `Die habe ich alle selbst erstellt`.
- Für Fotos JPG, für Zeichnungen / Grafiken eher GIF
- Weniger ist mehr

## 4 Teile deiner Webseite

### 4.1 Tabellen

#### 4.1.1 Die Definition

Die Tabellen in HTML werden mit Zeilen (`<tr></tr>`) und Spalten (`<td></td>`) aufgebaut. Diese Tags müssen zwischen den öffnenden und schliessenden `<table></table>`- Tag.
`<tr>` = Tabellenzeile
`<td>` = Tabellendaten

##### 4.1.1.1 Einfache Tabelle

Du brauchst für deine Webseite eine Tabelle mit vier Feldern:  zwei Zeilen à zwei Spalten.

```html
<table border="1">
    <tr>
        <td>Erste Zeile, erste Spalte</td>
        <td>Erste Zeile, zweite Spalte</td>
    </tr>
    <tr>
        <td>Zweite Zeile, erste Spalte</td>
        <td>Zweite Zeile, zweite Spalte</td>
    </tr>
</table>
```

Ergibt
![Table](./Bilder/Tabelle.png)

### 4.2 Der Link

Ein Link wird mit dem Tag `<a>` erstellt. Zwingendes Attribut für ein Link ist href=`adresse`.
`<a href="C:\Users\Schnupperlehrling\Desktop\Dateiname">Das ist ein Link!</a>`

#### 4.2.1 Einen Pfad kopieren

Um eine eigene zweite Website zu verlinken, kannst du auf die HTML-Datei mit Shift+Rechtsklick die Option `Als Pfad kopieren` auswählen.

![Pfad kopieren](./Bilder/Pfad%20kopieren.png)

### 4.3 Ein Bild einbinden

`<img src="C:\Users\Schnupperlehrling\Desktop\Dateiname">`
dort in den Quelltext einbinden, wo man das Bild haben möchte.

#### 4.3.1 Einen Bildpfad kopieren

Um ein lokales Bild auf deinem Computer zu verlinken, kannst du auf ein Bild mit `Shift+Rechtsklick` die Option `Als Pfad kopieren` auswählen.

![Bildpfad kopieren](./Bilder/Bildpfad%20kopieren.png)

Das Bild kannst du dann mit dieser Zeile einfügen.
`<img src="C:\Users\Schnupperlehrling\Desktop\background.jpg">`

### 4.4 Schriftart und Grösse definieren

```html
<h1 style="font-family:verdana;">Ich bin eine Überschrift</h1>
<p style="color:red; font-size:10px;">Ich bin ein Paragraph</p>
```

Auf die verschiedenen Textsorten können direkt `style` Attribute angewendet werden,
diese werden innerhalb von `style="…."` festgelegt und durch ein Semikolon (Strichpunkt) begrenzt.
Mit `color:` kann die Farbe des Textes verändert, mit dem `font-size:` Attribut wird die Grösse des Textes festgelegt und mit `font-family:` kann die Schriftart geändert werden.
Einen Absatz / eine Leerzeile kannst du mit `<p><br></p>` erstellen.  

## 5 Übung

Du hast die Theorie für die Erstellung deiner Webseite hinter dir. Nun kannst du in Visual Studio Code diese beiden Webseiten erstellen.

1. Seite
![Seite 1](./Bilder/Seite%201.png)

2. Seite
![Seite 2](./Bilder/Seite%202.png)
