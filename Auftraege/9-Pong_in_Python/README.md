# Pong mit Python

## 1 Aufgabenstellung

In diesem Auftrag wirst du den klassiker Pong mit Python und Pygame programmieren. Dies ist etwas komplexer als das Snake spiel. Schlussendlich solltest du das linke Paddle und das rechte vom Computer kontrolliert werden.

Am Ende wird das etwa so aussehen:

![Fertiges Spiel](./Bilder/fertiges_spiel.png)

## 2 Code Programmieren

Falls du noch eine Erklärung zu Python brauchst, schau am besten zuerst die vorherigen Aufträge mit Python nochmals an. Diese enthalten viele gute Informationen und lehren dir die Grundlagen.

### 2.1 Was ist Pygame

Pygame ist eine Library, also eine sammlung von Code, mit der in Python viele Tools bereitgestellt werden um Spiele zu programmieren. Beispielsweise wird Input-Handling und Rendering von Pygame übernommen. Dies reduziert die Schwierigkeit massiv und beschleunigt die Entwicklung. Somit kann man ganz einfach und schnell etwas zusammenbasteln ohne sich mit den sehr schweren Themen zu befassen müssen.

### 2.2 Wie wird Pygame verwendet

Pygame kann wie ein normales Modul einfach importiert werden, sofern es installiert ist. Wie jedes andere Modul ist dies dann direkt verfügbar. Über das Modul kann man dann auf mehrere verschiedene Aspekte zugreifffen. Dies wären beispielsweise Key-Events, 2D-Formen, Schriftarten oder Vektoren. Diese werden in den meisten Spielen verwendet werden.

## 3 Pong programmieren

Jetzt da du Pygame kennengelernt hast, kannst du nun mit dem programmieren beginnen. Öffne dazu als erstes die Python Datei im Ordner. Danach kannst du dir den Code durchschauen und schauen, was noch geändert werden muss. Wenn du den Link [Dokumentation](./Dokumentation/index.html) im Browser öffnest, werden dir die einzelnen Funktionen schön aufgelistet und erklärt.

### 3.1 move_player Funktion

Als erstes schreibst du die `move_player()` funktion. Diese schaut nach Input und bewegt dann den Spieler. Auch wird hier noch gerade geschaut, ob der Spieler mit Escape beenden will oder mit R den Ball zurücksetzen möchte.

In Pygame kannst du über `pygame.event.get()` alle speziellen Events abrufen. Darunter ist, ob der X Knopf des Fensters geklickt wurde. Mit einem loop kannst du dann überprüfen, ob das Spiel geschlossen werden sollte.

```Python
for event in pygame.event.get():
    if event.type == pygame.QUIT:
        pygame.quit()
        exit()
```

`Pygame.quit()` schliesst das Fenster und leert alles des Pygames Moduls. Mit `exit()` wird das Python Programm geschlossen.

Danach solltest du überprüfen, ob der Spieler Input gibt. Mit `pygame.key.get_pressed()` erhälst du eine Liste aller Buttons zusammen mit deren Status. Am besten nimmst du eine Variable, worin du diese Liste abspeicherst. Beispielsweise so:

```Python
keys = pygame.key.get_pressed()
if keys[pygame.K_UP]:
```

Da die Funktion teil einer Klasse ist, solltest du zum bewegen `self.move(paddle_speed)` verwenden.
Strukturiere den Rest der Funktion so.

#### Threads

Ein Thread ist ein Ablauf des Programmes. Jedes Python Programm beginnt in einem Thread gennant `__main__`. Du kannst im Programm einen neuen Thread erstellen, der eine Funktion durchführt. Mit der threading Library kannst du durch `thread = threading.Thread(target=function)` erstellst du einen Thread. Achte darauf, dass wenn du die Funktion ausführen wilst, dass du die Klammern der Funktion weglässt. Ansonsten wird die Funktion in diesem Thread ausgeführt. Diesen muss aber noch separat gestartet werden mit `thread.start()`.

Bei der Option um den Ball zurückzusetzen, musst du einen neuen Thread starten mit der Funktion `ball.start_pos_reset()`. Um zu verhindern, dass zu viele Threads gestartet werden, sollte der Thread nur erstellt werden, sofern kein anderer Thread läuft. Die Variable `thread_available` existiert bereits und wird auf `False` gesetzt innerhalb der Funktion. Strukturiere den Code nun so.

### 3.2 Check_Score Funktion

Um den Punktestand zu ändern, muss überprüft werden, ob der Ball auf einer Seite aufgeprallt ist. Über die Funktion `ball.rect.colliderect(objekt.rect)` kann getestet werden, ob sich zwei objekte überschneiden. Es existieren zwei unsichtbare Wände, `left_side` und `right_side`. Schreibe die Funktion so, dass die Variable mit dem Punktestand erhöht wird und dass ein Thread mit `ball.start_pos_reset()` gestartet wird.

### 3.3 Draw_Score Funktion

Hier programmierst du den Zähler für die Punktzahl. Zuerst musst du die Schrift definieren. Dies tust du mit `font = pygame.font.Font(Schriftart, Schfirtgrösse)`. Danach muss der Text definiert werden. Dies geschieht mit `text_variable = font.render("text", True, Farbe)`. Beachte, dass die Variable nicht direkt eingegeben werden kann. In Python, kannst du einen sogennanten "R-String" verwenden um den Wert einer Variable in einen Text einzufügen. Wenn du nun `f"{score_player}"` als Text verwendest, wird die Variable als Text interpretiert. Nun kannst du mit `screen.blit()` Den Text auf dem Bildschirm anzeigen. Verwende dazu `screen.blit(text_variable, (x,y))` um den Text an einem Bestimmten Ort anzuzeigen. Die Variablen `screen_height` und `screen_width` speichern die Dimensionen des Fensters ab.

## 4 Abschluss

Schlussendlich sollte das Spiel so aussehe wie auf dem Bild. Wenn du fertig bist, kannst du noch weitere Sachen einbauen oder umändern. Du kannst die Farbe ändern, Grösse und Schwierigkeit anpassen oder sonstiges noch einbringen. Experimentiere etwas herum.
