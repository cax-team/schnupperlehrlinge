import pygame
import random
import threading


def calculate_direction(ball,paddle):
    """
    Calculate angle at which a paddle pushes the ball
    :param ball:
    :param paddle:
    :return: direction as Vector2
    """
    ball_center = pygame.math.Vector2(ball.rect.center)
    paddle_center = pygame.math.Vector2(paddle.rect.center)
    direction = pygame.math.Vector2.normalize(ball_center - paddle_center)
    try:
        direction.scale_to_length(difficulties[current_difficulty]['speed'])
    except:
        pass
    return pygame.math.Vector2(direction)


def calculate_intersection(line1_start, line1_end, line2_start, line2_end):
    """
    Calculates the point of intersection between two lines
    :param line1_start: startpoint of first line
    :param line1_end: endpoint of first line
    :param line2_start: startpoint of second line
    :param line2_end: endpoint of second line
    :return: None if parallel
    :return: intersection points x and y
    """
    x1, y1 = line1_start
    x2, y2 = line1_end
    x3, y3 = line2_start
    x4, y4 = line2_end

    D = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)

    if D == 0:
        return None

    intersection_x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / D
    intersection_y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / D

    return int(intersection_x), int(intersection_y)


class Paddle:
    def __init__(self, x, y):
        self.rect = pygame.Rect(x, y, 15, difficulties[current_difficulty]['size'])

    def move(self, newpos):
        """Adjusts position in only vertical axis"""
        self.rect.y += newpos

    def draw(self):
        """Draws itself onto the screen"""
        pygame.draw.rect(screen, white, self.rect)



class Player_Paddle(Paddle):
    def move_player(self):
        """
        Gets Keypress Events and moves Paddle accordingly
        also handles exit and reset functionality
        """
        #####################
        # Funktion einfügen #
        #####################


class PC_Paddle(Paddle):
    def move_pc(self):
        """
        Calculates where the ball should be after each bounce and moves there if the ball should hit the back.
        Follows Ball movement otherwise
        """
        if 'offset' not in locals() or 'offset' not in globals():
            global offset
            offset = 0
        if last_bounce_entity in ["pc", "none"]:
            offset = random.randint(-50, 50)
        direction_line = pygame.math.Vector2(direction.x, direction.y)
        try:
            direction_line.scale_to_length(100)
        except:
            pass
        direction_line_end = ball.rect.center + direction_line
        threshhold = 20
        difference = self.rect.centery - ball.rect.centery
        back_line_start = (self.rect.x, 0)
        back_line_end = (self.rect.x, screen_height)
        reflection = tuple()
        if direction_line[1] < 0:
            reflection = calculate_intersection(ball.rect.center, direction_line_end, upper_wall.rect.bottomleft, upper_wall.rect.bottomright)
        elif direction_line[1] > 0:
            reflection = calculate_intersection(ball.rect.center, direction_line_end, lower_wall.rect.topleft, lower_wall.rect.topright)
        if reflection != ():
            direction_line = pygame.math.Vector2(direction_line[0], -direction_line[1])
            direction_line_end = reflection + direction_line
            reflection = calculate_intersection(reflection, direction_line_end, back_line_start, back_line_end)
            reflection = (reflection[0], reflection[1] + offset)
            reflection_difference = self.rect.centery - reflection[1]
            if reflection[1] > self.rect.centery and reflection[1] < screen_height and reflection[1] > 0 and abs(
                    reflection_difference) > threshhold:
                self.rect.y += difficulties[current_difficulty]['p_speed']
                return None
            elif reflection[1] < self.rect.centery and reflection[1] < screen_height and reflection[1] > 0 and abs(
                    reflection_difference) > threshhold:
                self.rect.y += -difficulties[current_difficulty]['p_speed']
                return None
            else:
                if abs(difference) < threshhold:
                    return None
                if difference > 0:
                    self.rect.y += -difficulties[current_difficulty]['p_speed']
                    return None
                elif difference < 0:
                    self.rect.y += difficulties[current_difficulty]['p_speed']
                    return None
        elif abs(difference) < threshhold:
            return None
        if difference > 0 and abs(difference) > threshhold:
            self.rect.y += -difficulties[current_difficulty]['p_speed']
            return None
        elif difference < 0 and abs(difference) > threshhold:
            self.rect.y += difficulties[current_difficulty]['p_speed']
            return None


class Ball:
    def __init__(self, x, y):
        self.rect = pygame.Rect(x, y, 10, 10)

    def move(self, direction):
        """
        Moves to Vector Position scaled by speed
        :param direction: Vector2 Position, where it should go to
        """
        try:
            direction.scale_to_length(difficulties[current_difficulty]['speed'])
        except:
            pass
        self.rect.x += direction.x
        self.rect.y += direction.y

    def draw(self):
        """Draws itself onto the screen"""
        pygame.draw.rect(screen, white, self.rect)

    def check_out_of_bounds(self):
        """Tests if position is inside a wall or goes straight up or down"""
        if self.rect.centery < 7 or self.rect.centery > screen_height-7 or direction.x == 0:
            if thread_available:
                thread = threading.Thread(target=self.start_pos_reset)
                thread.start()


    def check_score(self):
        """
        Adjusts Score on collision with the back side
        Resets the Field afterwards
        """
        #####################
        # Funktion einfügen #
        #####################




    def check_colission(self):
        """
        Checks if the ball has collided with another object.
        If it has been in contact with a paddle, it updates last_bounce_entity
        :return: Vector2 Direction
        """
        global direction
        global last_bounce_entity
        if self.rect.colliderect(paddle_player):
            direction = calculate_direction(self, paddle_player)
            last_bounce_entity = "player"
        elif self.rect.colliderect(paddle_pc):
            direction = calculate_direction(self, paddle_pc)
            last_bounce_entity = "pc"
        elif self.rect.colliderect(upper_wall) or self.rect.colliderect((lower_wall)):
            direction.y = -direction.y

        return direction

    def start_pos_reset(self):
        """
        Sets the position to the center and waits for the user to press "SPACE"
        """
        global thread_available
        if thread_available:
            thread_available = False
            global direction
            direction = pygame.math.Vector2(0,0)
            self.rect.center = (screen_width / 2, screen_height / 2)
            paddle_pc.rect.y = screen_height / 2 - difficulties[current_difficulty]['size'] / 2
            paddle_player.rect.y = screen_height / 2 - difficulties[current_difficulty]['size'] / 2
            while True:
                keys = pygame.key.get_pressed()
                if keys[pygame.K_SPACE]:
                    direction = pygame.math.Vector2(difficulties[current_difficulty]['speed'],0)
                    break
            thread_available = True
            return


class Wall:
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)

    def draw(self):
        """Draws itself onto the screen"""
        pygame.draw.rect(screen, white, self.rect)








def mainloop():
    """Main logic in this function for better structuring"""
    #draw_score()
    direction = ball.check_colission()
    paddle_player.move_player()
    if direction.x > 0:
        paddle_pc.move_pc()
    ball.check_score()
    ball.check_out_of_bounds()
    ball.move(direction)


def draw_objects():
    """Draw all objects to screen and lock paddles onto screen"""
    paddle_player.rect = paddle_player.rect.clamp(bounding_box)
    paddle_player.draw()
    paddle_pc.rect = paddle_pc.rect.clamp(bounding_box)
    paddle_pc.draw()
    ball.draw()
    upper_wall.draw()
    lower_wall.draw()


def draw_score():
    """Gets current score and displays it on the top"""
    #####################
    # Funktion einfügen #
    #####################


def show_main_menu():
    """Show Difficulty select screen"""
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
            pygame.quit()
            exit()

        screen.fill(black)
        global current_difficulty

        title_font = pygame.font.Font(None, 128)
        font = pygame.font.Font(None, 64)

        title_text = font.render("PONG", True, white)
        text_easy = font.render("Easy", True, black)
        text_medium = font.render("Medium", True, black)
        text_hard = font.render("Hard", True, black)

        button_size = pygame.font.Font.size(font, "Medium")

        button_easy = Wall(screen_width / 2 - button_size[0] / 2, button_size[1] + 130, button_size[0], button_size[1])
        button_medium = Wall(screen_width / 2 - button_size[0] / 2, button_size[1] * 3 + 130, button_size[0], button_size[1])
        button_hard = Wall(screen_width / 2 - button_size[0] / 2, button_size[1] * 5 + 130, button_size[0], button_size[1])

        screen.blit(title_text, (screen_width / 2 - title_text.get_size()[0] / 2, 50))

        button_easy.draw()
        button_medium.draw()
        button_hard.draw()

        screen.blit(text_easy, button_easy)
        screen.blit(text_medium, button_medium)
        screen.blit(text_hard, button_hard)

        mouse_x, mouse_y = pygame.mouse.get_pos()
        click, _, _ = pygame.mouse.get_pressed()

        if screen_width / 2 - button_size[0] / 2 <= mouse_x <= screen_width / 2 + button_size[0] / 2:
            if button_easy.rect.centery - button_size[1] / 2 <= mouse_y <= button_easy.rect.centery + button_size[ 1] / 2 and click:
                current_difficulty = 0
                return None
            if button_medium.rect.centery - button_size[1] / 2 <= mouse_y <= button_medium.rect.centery + button_size[1] / 2 and click:
                current_difficulty = 1
                return None
            if button_hard.rect.centery - button_size[1] / 2 <= mouse_y <= button_hard.rect.centery + button_size[1] / 2 and click:
                current_difficulty = 2
                return None

        pygame.display.flip()





if __name__ == "__main__":
    pygame.init()

    screen_height = 480
    screen_width = 680
    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption("Pong Game")

    white = pygame.Color(255, 255, 255)
    black = pygame.Color(0, 0, 0)

    paddle_speed = 5

    easy = {'speed': 6, 'size': 80, 'p_speed': 3}
    medium = {'speed': 8, 'size': 70, 'p_speed': 5}
    hard = {'speed': 10, 'size': 60, 'p_speed': 7}

    difficulties = [easy, medium, hard]
    current_difficulty = 0

    clock = pygame.time.Clock()

    paddle_player = Player_Paddle(screen_width / 6, screen_height / 2 - difficulties[current_difficulty]['size'] / 2)
    paddle_pc = PC_Paddle(5 * (screen_width / 6), screen_height / 2 - difficulties[current_difficulty]['size'] / 2)

    ball = Ball(screen_width / 2, screen_height / 2)

    upper_wall = Wall(0, 0, screen_width, 15)
    lower_wall = Wall(0, screen_height - 15, screen_width, 15)

    bounding_box = Wall(0, 0, screen_width, screen_height)

    left_side = Wall(0, 0, 15, screen_height)
    right_side = Wall(screen_width - 15, 0, 15, screen_height)

    direction = pygame.math.Vector2(difficulties[current_difficulty]['speed'], 0)

    score_player = 0
    score_pc = 0

    last_bounce_entity = "none"

    show_main_menu()

    thread_available = True

    while True:
        screen.fill(black)
        mainloop()
        draw_objects()
        pygame.display.flip()
        clock.tick(60)