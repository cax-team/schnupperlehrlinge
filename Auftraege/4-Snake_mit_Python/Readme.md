# Python Spiel

## 1 Aufgabenstellung / Ziel

Dein Auftrag ist es, den Code des Snake Spiels fertig zu erstellen. Er muss möglichst genau nach Anleitung sein und lediglich funktionieren. Schlussendlich sollte das Game etwa so aussehen:

![Fertiges Spiel](./Bilder/fertiges%20spiel.png)

## 2 Planung

### 2.1 Vorüberlegungen

Bevor man sich vor den Computer setzt, Visual Studio Code startet und einfach mal den unfertigen Code ausführt, sollte man sich etwas Zeit nehmen, um seinen Code zu planen.
Dazu sollte man sich folgendes überlegen:

#### 2.1.1 Wie soll das ganze einmal ablaufen

##### 2.1.1.1 So sicher nicht

Hier ein Auszug, was man sicher nicht wählen sollte:

- Einfach schnell und unsauber arbeiten
- Ohne Plan vorangehen

##### 2.1.1.2 Schon eher

- Sich klar überlegen, wie der Code aussehen könnte
- Sich einen Plan machen
- Wenn unklar, Fragen stellen

## 3 Einführung in Python

Wichtig: Speichere deine Arbeit regelmässig, damit bei einem Absturz möglichst wenig verloren geht.

### 3.1 Was ist Python Pygame

Pygame ist eine spezielle Software, die es Entwicklern ermöglicht, Spiele und Animationen zu erstellen. Es ist so etwas wie ein Werkzeugkasten, der viele nützliche Dinge enthält, die benötigt werden, um Spiele zu machen. Mit Pygame können Entwickler Bilder, Töne und Bewegungen in ihren Spielen verwenden. Es ist so ähnlich wie das Malen auf einem Computer, aber mit vielen zusätzlichen Möglichkeiten. Wenn du jemals davon geträumt hast, dein eigenes Spiel zu machen, könnte Pygame dir dabei helfen, diesen Traum wahr werden zu lassen.

### 3.2 Der Aufbau von so einem Pygame mit Python

Python Pygame basiert auf einer Bibliothek namens Pygame, die zuerst importiert werden muss. Der Importbefehl import pygame ermöglicht den Zugriff auf alle Funktionen und Werkzeuge von Pygame. Hier ist ein Beispiel, wie der Import in deinem Code aussehen würde:

```Python
import pygame

# Restlicher Code hier... 

```
Beispiel:

![Import](./Bilder/import.png)


Der weitere Code für ein Pygame-Spiel besteht aus verschiedenen Schritten. Zuerst importierst du das Pygame-Modul. Dann initialisierst du das Spiel und erstellst ein Fenster. Eine Schleife hält das Spiel am Laufen, während du Benutzereingaben verarbeitest, den Spielzustand aktualisierst, Grafiken rendertest und Kollisionen überprüfst. Du kannst diese Schritte anpassen, um die spezifischen Anforderungen deines Spiels zu erfüllen. Die Schleife wiederholt sich, bis das Spiel beendet wird. Dieser Aufbau ermöglicht es dir, interaktive Spiele mit Pygame zu entwickeln und deine Kreativität auszuleben.
So ein weiterer Code kann sehr gross werden. Hier ein Beispiel:

![Beispiel](./Bilder/grosses%20beispiel.png)

Am Ende eines Pygame-Spiels gibt es einige wichtige Schritte, die durchgeführt werden sollten:

1. Beenden des Pygame-Moduls: Um das Pygame-Modul ordnungsgemäß zu beenden und alle Ressourcen freizugeben, solltest du `pygame.quit()` aufrufen. Dadurch werden alle geöffneten Fenster geschlossen und alle Pygame-Module beendet.
2. Beenden des Python-Programms: Nachdem das Pygame-Modul beendet wurde, kannst du dein Python-Programm beenden, indem du `sys.exit()` aufrufst. Dadurch wird das Programm sofort beendet und der Code danach wird nicht mehr ausgeführt.

Diese Schritte gewährleisten eine saubere Beendigung des Spiels und sorgen dafür, dass alle Ressourcen freigegeben werden. Es ist wichtig, diese Schritte am Ende deines Pygame-Spiels zu implementieren, um potenzielle Speicherlecks oder andere Probleme zu vermeiden.

## 4 Die Entwicklung des Programms

### 4.1 Python-Datei öffnen

Öffne ‘Snake Schnuppis.py’ «mit Visual Studio Code».

Im Fenster, das sich geöffnet hat, kannst du den Code des Spiels sehen. Der Code ist leider nicht fertig, das heisst du sollst ihn fertig schreiben. Über den ganzen Code verteilt siehst du grüne Sätze (das sind Kommentare), die genau beschreiben was zu tun ist, oder welcher Code benötigt wird.

![unfertiger Code](./Bilder/unfertiger%20code.png)

Damit du das Pygame und alles andere importieren kannst, brauchst du die richtigen Codes. Diese Codes solltest du mit der Beschreibung des Aufbaus selbst herausfinden können. Wenn du das erledigt hast, kommst du zu der Bestimmung der Geschwindigkeit von der Schlange.

![Geschwindigkeit](./Bilder/kommentare.png)

Nun können wir programmieren. Jeglichen Code schreibt man immer unter den Kommentaren «grüne Hilfssätze». Hier gibst du nun folgendes ein, wenn du das mit dem Importieren hinbekommen hast:

`snake_speed = 15` -> bestimme selbst wie schnell deine Schlange sein soll

Nun hast du die Geschwindigkeit eingestellt und kannst jetzt die Grösse des Bildschirms programmieren, der erscheint, nachdem man das Game ausführt.

`window_x =`   -> Hier würde ich dir 820 empfehlen
`window_y =`  -> Hier würde ich dir 680 empfehlen

So jetzt hast du den Screen und den Speed programmiert, nun brauchst du noch einen Code für die Farben:

![Farben](./Bilder/farben.png)

Diesen sollst du genau wie auf dem Bild übernehmen.

Das war jetzt der leichte Teil der Arbeit. Nun kommen längere und kompliziertere Codes. Hier musst du drauf achten, dass du keine Rechtschreibfehler machst, ansonsten entstehen Fehlermeldungen.

Wenn du weiter runter scrollst, musst du den Körper der Schlange konfigurieren. Weil ohne Körper, ist auch keine Schlange zu sehen.

Hier ein Beispiel:

```Python
snake_body = [[100, 50],
            [90, 50],
            [80, 50],
            [70, 50]
            ]
```

Da die Schlange jetzt komplett erstellt wurde, musst du die nur noch zum Laufen bringen. Das geht, indem du die Bewegung programmierst. Wenn du fast ganz nach unten scrollst, findest du den Kommentar für die Bewegung.

![Bewegung](./Bilder/steuerung.png)

Jetzt läuft die Schlange garantiert.

Zuletzt musst du noch den Screen für die Punktzahl (Score) entwerfen.

```Python
def show_score(choice, color, font, size):

score_font = pygame.font.SysFont(font, size)

score_surface = score_font.render('Deine Punktzahl : ' + str(score), True, color)

score_rect = score_surface.get_rect()

game_window.blit(score_surface, score_rect)
```

## 5 Die Benutzung des Programms

Da du jetzt den ganzen Code vervollständigt hast, kannst du ihn jetzt ausführen.

![Start](./Bilder/start.png)

Du kennst es ausführen, indem du auf den Pfeil oben rechts drückst.

![Fenster](./Bilder/fertig.png)

Dann sollte direkt das Fenster aufgehen:

## 6 Erweiterungsvorschläge

Wenn du genügend Zeit hast, kannst du noch einige Funktionen ändern. Falls du hier Fragen hast, unterstützt dich deine Aufsichtsperson gerne.

- Ändere den Namen des Games.
- Stelle ein, dass es statt 10 Punkte nur 1 Punkt gibt.
