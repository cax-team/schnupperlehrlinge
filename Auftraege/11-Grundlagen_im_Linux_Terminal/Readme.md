# Grundlagen von Linux Terminal in einer Virtual Machine

## 1 Aufgabenstellung

In dieser Aufgabe wirst du Linux Ubuntu in einer VM starten. In dieser wirst du einige einfache Aufgaben lösen umso einen ersten Einblick im Umgang mit Linux und dem Terminal zu bekommen. Alle Befehle, sind in der Befehlsliste angegeben.

## 2 Einführung

### 2.1 Was ist eine Virtual Machine

Einfach gesagt, ist eine Virtual Machine (VM) eine Art virtueller Computer innerhalb deines Computers. Die VM ist wie ein zweiter Computer welcher auf deinem eigentlichen Computer lauft.  Normalerweise braucht man VMs um Projekte virtuell zu testen, bevor man sie dann in der Praxis ausführt.

### 2.2 Was ist Linux Ubuntu

Linux Ubuntu ist ähnlich wie Windows oder MacOS, ein Betriebssystem. Es gibt verschiedene Arten von Linux Betriebssysteme. Diese nennt man Linux Distributionen. Ubuntu ist eine davon. Linux gilt als sehr stabil und zuverlässig. Daher wird es in der Informatik oft für Server in Rechenzentren verwendet. Auch du wirst in der Informatik Lehre mit diesem Betriebssystem zu tun haben.

### 2.3 Was ist das Terminal

Das Terminal in Linux ist ein Programm, mit dem man über Textbefehle direkt mit dem Betriebssystem kommunizieren kann. Anstatt mit der Maus auf Symbole zu klicken, gibst du spezielle Befehle ein, um Aufgaben auszuführen, Programme zu starten, Dateien zu verwalten oder Systemeinstellungen zu ändern. Das Terminal ermöglicht eine schnelle und effiziente Steuerung des Computers.

## 3 Vorbereitung

### 3.1 Virtual Machine starten

Öffne das Programm «Oracle VM VirtualBox»/«Oracle VirtualBox».

Starte die Virtual Machine mit Linux Ubuntu:

![VMStarten](./Bilder/VMStarten.png)

### 3.2 Terminal öffnen

Wenn Linux Ubuntu ganz aufgestartet ist drücke `Ctrl + Alt + T` um in das Terminal zu gelangen

## 4 Befehlsliste

Hier findest du alle Befehle, welche du für die Aufgaben benötigst!

### 4.1 Dateien und Verzeichnisse navigieren:

Dateien und Ordner im aktuellen Verzeichnis auflisten: `ls`

Verzeichnis wechseln: `cd Verzeichnisname`

Zum Beispiel «cd Dokumente» wechselt in das Verzeichnis «Dokumente»

### 4.2 Dateien und Verzeichnisse manipulieren

Eine neue leere Datei erstellen: `touch Dateiname`

Datei in angegebens Verzeichnis verschieben: `mv Dateiname Verzeichnispfad`

Eine Datei löschen: `rm Dateiname`

Ein neues Verzeichnis erstellen: `mkdir Verzeichnisname`

Den Inhalt der ersten datein in die zweite Datei kopieren: `cp Dateiname1 Dateiname2`

Den Inhalt Sortieren und in eine andere Datei verschschieben: `sort Dateiname1 -o Dateiname2`

### 4.3 Textdateien anzeigen und bearbeiten

Inhalt der Datei im Terminal anzeigen: `cat Dateiname`

Einfache Textbearbeitung im Terminal öffnen: `nano Dateiname`

Datei umbenennen: `mv alterName neuerName`

### Benutzer erstellen

Einen neuen Benutzer erstellen: `sudo adduser benutzername`<br>
Das Passwort von [sudo] schnupperli ist: Schnupperlehre

## 5 Aufgaben

### 5.1 Benutzer erstellen

Deine Aufgabe besteht nun darin mithilfe des Terminals einen neuen Benutzer zu erstellen. Wenn du dich im Terminal befindest, kannst du mithilfe der oberen Befehlsliste, die Aufgabe lösen.

1. Erstelle einen neuen Benutzer. Den Namen darfst du selber wählen.<br> 
**Wichtig!**

**Der Benutzername muss aus Kleinbuchstaben bestehen!<br>Falls du bei der Passworteingabe nichts siehst: Keine Angst, das Passwort wird trotzdem eingegeben**

2. Erstelle ein neues Passwort für den Benutzer

3. Gib den vollständigen Namen für den Benutzer ein

4. Informationen wie Zimmernummer, Telefon oder Sonstiges kannst du auslassen

5. Verlasse danach das Terminal und suche in den Einstellungen nach Benutzer. Wenn der erstellte Benutzer dort erscheint, hast du alles richtig gemacht.

### 5.2 Verzeichnisse erstellen und verschieben

Erstelle ein neues Verzeichnis mit dem Namen «Schnupperlehre» auf deinem Schreibtisch.

1. Wechsel in das Verzeichnis «Schreibtisch»

2. Erstelle darin eine Datei. Den Namen darfst du selber wählen

3. Überprüfe mit `ls` ob die Datei erstellt wurde

4. Erstelle das Verzeichnis «Schnupperlehre»

5. Verschiebe die Datei in das Verzeichnis «Schnupperlehre»

6. Gehe in das Verzeichnis «Schnupperlehre» und überprüfe ob sich die Datei dort befindet


### 5.3 Eine Datei in einer anderen Datei speichern

Kopiere den Inhalt einer Datei in eine andere.

1. Gehe in das Verzeichnis «Schnupperlehre»

2. Erstelle zwei Dateien mit den Namen «Quelle» und «Ziel»

3. Überprüfe ob beide Dateien erstellt wurden

4. Öffne die Datei «Quelle» mit der Textbearbeitung im Terminal

5. Schreibe in die Datei Quelle.txt irgendeinen Text.
Zum Beispiel: «Aveniq Schnupperlehre Aufgabe».

6. Speicher die Datei. `Ctrl + X`-> `Y`-> `Enter`

7. Kopiere den Inhalt von der Datei «Quelle» in die Datei «Ziel»

8. Lies «Ziel» im Terminal aus, um sicherzustellen, dass der Inhalt kopiert wurde.

9. Lösche die Datei «Quelle»


### 5.4 Dateiinhalt sortieren

In dieser Aufgabe lernst du, wie du den Inhalt einer Datei sortierst.

1. Erstelle die Dateien «unsortiert» und «sortiert» auf deinem Schreibtisch

2. Schreibe mit der Textbearbeitung verschiedene Buchstaben untereinander in das Dokument «unsortiert.

    Zum Beispiel:

    ![unsortiert](./Bilder/unsortiert.png)

3. Gehe danach wieder zurück zum Terminal

4. Sortiere den Inhalt der Datei alphabetisch und speichere ihn in die Datei «sortiert»

5. Zeige den Inhalt der sortierten Datei im Terminal an

## 6 Beenden

Wenn du mit allen Aufgaben fertig bist, fahre die Machine herunter und schliesse das Programm