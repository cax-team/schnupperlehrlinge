# Künstliche Intelligenz

## 1 Einführung

In diesem Teil deiner Schnupperlehre hast du die Möglichkeit einige Erfahrungen mit einer künstlichen Intelligenz (AI) zu sammeln. Die zur Verfügung gestellten AI’s, erstellen aus einem von dir geschriebenen Text ein Bild. Wenn du z.B. `mountain` eingibst, generiert dir die AI ein Bild oder mehrere Bilder eines Bergs. Dies funktioniert wie folgt: Die AI erkennt aus dem Text verschiedene Elemente z.B. `mountain` wie oben, `river` oder `lake`. Zu diesen Elementen werden Bilder gesucht und dann miteinander kombiniert. Bei Landschaftsbeschreibungen entsteht daraus meistens ein realistisches Bild.
(siehe später im Auftrag)

### 1.1 Hilfsmittel

Links zu den Websites:

- [deepai.org](https://deepai.org/machine-learning-model/text2img)

- [stablediffusionweb.com](https://stablediffusionweb.com/#demo)

## 2 Erstellen eines Bildes mit der AI

Du kannst nun eine der bei Hilfsmittel aufgeführten Webseiten verwenden.

Bei [deepai.org](https://deepai.org/machine-learning-model/text2img):
Wenn du die Webseite öffnest, sollte dieses Interface sichtbar sein.

![DeepAI Homepage](./Bilder/DeepAI%20hauptseite.png)

Im weissen Eingabefeld kannst du nun einen Text eingeben. Eine Möglichkeit für den Beginn wäre `mountain with river`.

Wenn du deinen gewünschten Text eingegeben hast, kannst du unten auf den blauen Button `Generate` klicken.
Die künstliche Intelligenz wird einen Moment brauchen, bis das Bild generiert wird. Bei Fertigstellung wird das generierte Bild unter dem Button `Generate` angezeigt.

Beispiel für ein generiertes Bild (mountain with river):

![Mountain with River Beispiel](./Bilder/mountain%20with%20river.png)

Bei [stablediffusionweb.com](https://stablediffusionweb.com/#demo):

Wenn du diese Webseite öffnest, sollte dieses Interface sichtbar sein.

![Stable Diffusion Homepage](./Bilder/Stable%20Diffusion%20hauptseite.png)

Du musst nach unten scrollen, um zur AI zu kommen.

![Stable Diffusion Playground](./Bilder/Stable%20Diffusion%20playground.png)

Du kannst einen Text eingeben, wenn du bei `Stable Diffusion Playground` ankommst. Hier kannst du z.B. wieder den Text `mountain with river` eingeben oder du hast selber eine Idee und gibst einen eigenen Text ein.

Um das Bild zu generieren, klickst du auf `Generate image`.

Beispiel für generierte Bilder (mountain with river):

![Mountain with River 2](./Bilder/mountain%20with%20river%202.png)

## 3 **Weitere Ideen**

Dies wären noch einige Möglichkeiten für Texteingaben in den AI’s:

- ocean with palms
- lake with forest
- cow on lake
- desert with snow
- cat fighting a dog
- people on a ship
- sunrise in antarctica
- shooting star over lake
- skiing cat
- seal playing football
- robot on a spaceship
