# Powershell Script

## 1 Aufgabenstellung / Dein Ziel

In diesem Auftrag wirst du mit PowerShell ein kleines Skript schreiben. Dein Auftrag ist es, ein Skript zu realisieren, dass eine Ordnerabfrage macht. Es wird getestet, ob ein Ordner `test` in deinem Ordner `Vorlagen` vorhanden ist. Da dieser nicht vorhanden ist erstellst du eine Meldung, die angezeigt werden sollte. Zudem erstellst du im Skript den gewünschten Ordner `test`. Damit dein Skript auch funktioniert, wenn der Ordner vorhanden ist, schreibst du auch eine Meldung dafür. Dies machst du mit der Entscheidungsstruktur `If {…} und Else {…}`. Wie bereits beim HTML-Auftrag wirst du zuerst die Theorie durchlesen und dann das Skript schreiben.

## 2 Der Aufbau des Skripts

Für das Skript arbeitest du im bereits bekannten Programm `Visual Studio Code`.

### 2.1 Erstellung der Skript-Datei

Öffne wieder `Visual Studio Code`.

![VSCode öffnen](./Bilder/VSCode.png)

Um ein Dokument zu erstellen, kannst du wieder oben links auf `File` klicken und dann `New Text File`.

![Neue Datei erstellen](./Bilder/Neue%20datei%20erstellen.png)

 Im Fenster, das sich geöffnet hat, kannst du auf `Select a language` klicken. Im geöffneten Suchfeld gibst du `PowerShell` ein und klickst Enter. Du hast nun deine Datei für das Skript erstellt. Damit deine Arbeit nicht verloren geht, solltest du die PowerShell-Datei speichern. Dafür gehst du wieder auf `File` und klickst dann auf `Save`. Speichere die Datei in deinem Ordner `Vorlagen` auf dem Desktop.

 ![Datei abspeichern](./Bilder/Datei%20abspeichern.png)

```Powershell
Add-Type -AssemblyName System.Windows.Forms      
Add-Type -AssemblyName System.Drawing                   

$path = Pfad zum Ordner, wenn er vorhanden wäre
$newpath = wo der neue Ordner gespeichert werden soll


If (Test-Path $path) #Test, ob der Ordner in diesem Verzeichnis vorhanden ist
{
    [System.Windows.Forms.MessageBox]::Show("Deine Meldung","Titel der Messagebox",0)
}

Else
{
   
   #Wenn der Ordner nicht vorhanden ist, wird ein Ordner erstellt

   Befehl für die Erstellung des Ordners

   [System.Windows.Forms.MessageBox]::Show("Deine Meldung","Titel der Messagebox",0)
}
```

*Erläuterung Variablen ($...):*

- Variablen dienen dazu, um einen gewissen Wert zu speichern. In unserem Beispiel definierst du die Variable `$path="…"`. Dies erspart dir vieles Tippen, da du nicht jedes Mal, wenn du den Pfad brauchst, den ganzen Pfad schreiben musst.

*Erläuterung If Else:*

- Bei If und Else findet eine Bedingungsprüfung statt. If (wenn) eine Bedingung zutrifft, dann mach das. Else (sonst) mach das.

*Erläuterung Messagebox:*

- Der Befehl `[System.Windows.Forms.MessageBox]::Show("Deine Meldung","Titel der Messagebox",0)` erstellt dir eine Messagebox. Eine Messagebox sieht so aus:

![Message Box](./Bilder/Message%20Box.png)

## 3 Schreiben des Skripts

Für das Schreiben des Skripts kannst du dich am obigen Aufbau orientieren. Diese beiden Befehle kannst du schon mal ganz oben in dein Skript reinschreiben, da sie nötig sind für die Erstellung der Messageboxen in Visual Studio Code.

```Powershell
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
```

Nun geht es weiter mit der Variablendeklaration. Wir gehen davon aus, dass der Ordner `test` heissen würde. Die Variable $path würde somit `"C:\Users\Schnupperlehrling\Desktop\Vorlagen\test"` entsprechen. Deklariere nun diese Variable. Da wir in unserem Fall wissen, dass der Ordner `test` nicht vorhanden ist, definieren wir eine zweite Variable. Die Variable $newpath entspricht dem Pfad, wo du den Ordner `test` erstellen möchtest. Dies wäre `"C:\Users\Schnupperlehrling\Desktop\Vorlagen"`. Erstelle auch diese Variable.
Den Teil

```Powershell
If (Test-Path $path) 
    {
        [System.Windows.Forms.MessageBox]::Show("Deine Meldung","Titel der Messagebox",0)
    }
```

kannst du übernehmen. Allerdings solltest du "Deine Meldung" und "Titel der Messagebox" anpassen. Bei "Deine Meldung" kannst du z.B. "Der Ordner ist bereits vorhanden" schreiben. Bei "Titel der Messagebox" z.B. "Ordnerabfrage".

Nun kommen wir zu Else. Hier definierst du, was passieren soll, wenn der Ordner `test` noch nicht vorhanden ist. Wie bereits beschrieben möchten wir bei Else den Ordner `test` erstellen. Den Ordner erstellst du mit dem Befehl: `New-Item -Name "test" -Path $newpath -ItemType directory`
Da wir auch noch eine Meldung ausgeben wollen, erstellen wir eine weitere Messagebox. Dieses Mal im Else-Statement. Du kannst also wieder

```Powershell
[System.Windows.Forms.MessageBox]::Show("Deine Meldung","Titel der Messagebox",0)
```

eingeben und dann "Deine Meldung" und "Titel der Messagebox" anpassen. Bei "Deine Meldung" kannst du z.B. "Der Ordner ist nicht vorhanden. Er wurde nun erstellt." schreiben. Bei "Titel der Messagebox" z.B. wie zuvor "Ordnerabfrage".

*Wenn du damit fertig bist, kannst du dich bei deinem Betreuer/deiner Betreuerin melden. Er/Sie wird das Skript kurz durchschauen und dir zeigen, wie du das Skript ausführst.*
