# 10-Webseite_mit_Vuejs

## 1 Aufgabestellung

In diesem Auftrag wirst du eine Interaktive Webseite mit Vue.js Erstellen. 

Am ende Wird es Etwa so Aussehen:

<img src="./Bilder/Screenshot%202023-10-13%20092114.png" alt="Fertige Webseite" width="1000">

## 1.1 Was ist Vue.js

Vue.js ist ein JavaScript-Framework für die Erstellung interaktiver Webseiten und Webanwendungen. Es macht die Entwicklung von Webanwendungen einfacher und effizienter.

## 2 Projekt Starten
Erstmals muss du, wenn noch nicht vorhanden, Node.js und Git installieren. Alle Einstelluingen beim Installieren sollst du auf standart lassen.
Um das Projekt zu starten musst du alle Folgene Commands in richtiger Reienfolge nach einander im Terminal eingeben.
```bash
git clone https://gitlab.com/cax-team/schnupperlehrlinge.git
cd ./schnupperlehrlinge/Auftraege/10-Webseite_mit_Vuejs
npm install
npm run dev
```
Wenn alle Aufträge gelöst sind gehtst und ins Powershell und machst folgende Commands:
```bash
cd "$env:userprofile\OneDrive - Aveniq AG\Documents\Devops\schnupperlehrlinge\Auftraege\10-Webseite_mit_Vuejs"
Remove-Item "node_modules"
```
```js
<script setup>
```
## 3 Konfetti Erstellen 

Damit man den Efekt hinbekommt Konfetti erscheinen zu lassen muss man erst js-confetti Instalieren dies wird gemacht im terminal mit dem code: npm add js-confetti. 

### 3.1 js-confetti importieren 

Nun müsse wir js-confetti importieren. Dies wird mit dem Befel Import gemacht. 
```Js
import JSConfetti from 'js-confetti'
import { ref } from 'vue'
const confetti = new JSConfetti()
```
Wichtig dabei ist es, dass wir nicht von 'vue' sonder von 'js-confetti' importieren, da wir es Installiert haben und nicht schon in vue inbegriffen war.

### 3.2 Funktion Konfetti

Als nächstes müssen wir einen Weg finden Konfetti erscheinen zu lassen. Dies machen wir mit dem Befel: 
```js
function showConfetti() {
}
```
Die funktion ist dafür da, dass das Programm weis, das wir etwas auslösen wollen. Das 'showConfetti' ist nur dafür da, damit wir es abrufen können. Du kanst dies so nennen wie du Willst Z.b. älplermagronen oder Rösti... 
Unter die Funktion muss ein Befehl sein damit das Programm etwas macht.
In diesem Falle ist es:
```js
  confetti.addConfetti({
    emojis: ['', '', ''] // füge hier Emojis deiner Wahl ein
  })
```
Ausserdem brauchen wir noch en const:
```js
const confetti = new JSConfetti()
```

### 3.3 Button Erstellen

Jetzt brauchen wir nur noch ein Button welcher das ganze auslöst. Den button machen wir mit:
```js
<button @click="showConfetti">{{ "" }}</button> 
```
In die geschweiften Klammern kannst du nach Wunsch irgendetwas hineintun. 
Probiere jetzt aus, den Button nach Wunsch grösser, kleiner oder in die Mitte zu zentrieren.


## 4 if, else switch

Dies ist ein if, else switch, welcher je nach input den Text ändert.
```js
<h1 v-if="awesome">Beispieltext</h1>
<h1 v-else>Beispieltext2</h1>
```

Damit dies Funktioniert braucht man eine Funktion. Die kann so aussehen:
```js
function toggle() {
  awesome.value = !awesome.value
}
```
ausserdem Brauchst du diesen Code
```js
const awesome = ref(true)
```

## 5 Hintergrungfarbe

Nun ändern wir noch die Hintergrundfarbe. Ich habe dies auch mit Buttons und Funktionen gemacht, damit man sie mit einem Knopfdruck ändern kann.
```js
function red() {
  redColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "red"
}
```
Ausserdem muss dieser Code im CSS eingefügt werden
```css
body {background-color:red;}
```
```js
const redColor = ref(0)
```
Versuche jetzt, den Button selber zu erstellen, achte auf die richtige Verknüpfung.

### 5.1 Selbständige Buttons

Nun hast du alles gelernt das du wissen musst. Um selbständig Buttons hinzuzufügen. Probiere jetzt ein bischen aus mit Verschiedenen Farben oder Funktionen.

## 6 Design der Webseite

Dieser Schritt ist optional, oder du kannst dies auch selbst nach Bedarf machen. Dieser Schritt wird meistens während der Entwicklung des anderen Codes gemacht.
```CSS
h1 {
  font-size: xxx-large;
  text-align: center;
  margin-top: 1em;
}
button {
  display: block;
  margin: 0 auto;
  cursor: pointer;
  margin-top: 3em;
}
a {
  text-align: center;
}
```
Dies wird im CSS gemacht, und für jede Gruppe (Button, h1, a usw.), die wir ändern möchten, erstellen wir einen eigenen Abschnitt. Dadurch wird ermöglicht, dass nicht alles dieselben Eigenschaften hat, sondern auch variieren kann.
```
font-size:...;
```
font-size wird für die Größe des Textes benutzt. Normalerweise verwendet man "px" als Maßeinheit.
```
text-align:...;
```
text-align wird benutzt, um den Text auf der X-Achse zu positionieren.
```
margin-top:...;
```
margin-top wird benutzt, um den Abstand vom oberen Rand zu definieren.
```
cursor: pointer;
```
Dies wird benutzt als visueller Effekt, damit der Cursor zu einem Zeiger wird, wenn er sich über einem Button befindet.

## Lösung

Hier findest du die lösung fals du nicht weiter kommst.
[Lösung](./src/Lösung/README.md)