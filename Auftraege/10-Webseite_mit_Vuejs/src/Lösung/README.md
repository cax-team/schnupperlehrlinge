# Lösung Webseite mit Vue.js
```js
<script setup>

import JSConfetti from 'js-confetti'
import { ref } from 'vue'
const confetti = new JSConfetti()
const awesome = ref(true)
const redColor = ref(0)
const greenColor = ref(0)
const blueColor = ref(0)
const whiteColor = ref(0)
const blackColor = ref(0)





// diese Funktion ändert von if zu else je nach input
function toggle() {
  awesome.value = !awesome.value
}
// die folgenden funktionen sind für die Farbänderung zuständig
function red() {
  redColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "red"
}

function blue() {
  blueColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "blue"
}

function green() {
  greenColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "green"
}

function white() {
  greenColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "white"
}
 
function black() {
  greenColor.value++
  var x = document.getElementsByTagName("BODY")[0];
  x.style.backgroundColor = "black"
}

// diese Funktion lässt Konfetti erscheinen

function showConfetti() {
  confetti.addConfetti({
    emojis: ['🌈', '⚡️', '💥', '✨', '💫', '🌸']
  })
}
</script>

<template>

 
  <h1>🎉 This is en Example for an interactive Website you can make.</h1>
  <!-- Dies ist ein Button, der beim Klicken die Funktion "showConfetti" auslöst -->
  <button @click="showConfetti">{{ "💩" }}</button> 
  <!-- dies ist ein if/else command welcher je nach input den text wechselt -->
  <h1 v-if="awesome">🤓</h1>
  <h1 v-else>🥸</h1>
  <button @click="toggle">{{ "🤡" }}</button> 
  <!-- Dies sind Buttons, welche das signal red, blue... den funktionen senden-->
  <button @click="red">{{ "🟥" }}</button> 
  <button @click="blue">{{ "🟦" }}</button> 
  <button @click="green">{{ "🟩" }}</button> 
  <button @click="white">{{ "⬜" }}</button> 
  <button @click="black">{{ "⬛" }}</button> 

</template>

<style>
h1 {
  font-size: xxx-large; /* vergrössert den Text */
  text-align: center; /* Zentriert den Text */
  margin-top: 1em; /* Passt den Abstand an */
}
button {
  display: block;  /* Stellt sicher, dass der Button sichtbar ist*/ 
  margin: 0 auto;  /* Zentriert den Button */
  cursor: pointer; /* ändert den Cursor */
  margin-top: 3em;  /* Passt den Abstand an */
}
a {
  text-align: center; /* Zentriert den Text */
}

/* Dies ist ein teil von der änderung der hintergrundfarbe */
body {background-color:red;}


</style>
```