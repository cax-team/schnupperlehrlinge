# Webrequest mit Python

## 1 Einstieg Python

In diesem Auftrag arbeitest du mit der Programmiersprache `Python`. Möglicherweise kennst du diese bereits. Falls nicht, gibt es hier ein paar Erklärungen.

1. Funktionen werden mit Tabs geordnet. (*Significant Whitespace*)
2. Eine if Klausel sieht so aus. Sie wird nur ausgeführt, wenn die Kondition erfüllt ist

    ```Python
    if x == 10:
        print("Hallo!")
    ```

3. Python arbeitet mit Objekten, welche auch deren eigene Funktionen haben können. Diese kann man durch Object.Funktion() aufrufen.
4. Variablen werden so definiert   `x = 5`

Anmerkung:
Weitere Hilfe zu Python gibt es reichlich im Internet.
Sonst kann dir eine Betreuungskraft weiterhelfen.
Mögliche Kombination der Befehle für eine kleine Funktion:

```Python
for n in range(10):     #Mach das 10 Mal
    print("Hallo")      #Was er 10 Mal tun soll
```

Mit der `range()` Funktion wird eine liste von Zahlen erstellt. Diese beginnt standardmässig bei 0 und endet bei der angegebenen Zahl. Die Zahl selbst (also 10) ist nicht mehr in dieser Liste vorhanden.

## 2 Aufgabe

In Python solltest du nun versuchen eine kleine App zu programmieren. Hier wirst du einiges kennenlernen, was du auch im Betrieb antreffen könntest. Schlussendlich sollte das Programm so aussehen:

![Fertiges Programm](./Bilder/Fertige%20App.png)

Als erstes kopiere die Vorlage Webrequest.py und background1-3.png auf den Desktop.

![Vorlagen](./Bilder/Vorlagen.png)

![Kopieren](./Bilder/Kopieren.png)

Öffne danach App.py mit Visual Studio Code.  

![VSCode](./Bilder/VS%20Code%20oeffnen.png)

Wenn ein Popup im unteren rechten Rand auftaucht, klicke auf Install und warte, bis es abgeschlossen ist.

![Extension](./Bilder/Extension%20installieren.png)

Danach bist du bereit zu arbeiten.
Deine Aufgabe ist es jetzt, eine Funktion zu schreiben, welche die Abfrage der Website aufbereitet, um diese dann im Fenster korrekt anzuzeigen. Die Website gibt die Antwort als JSON aus. JSON ist ein standardisiertes Format, welches Datentypen zu Werten verknüpft. Es lässt sich leicht damit arbeiten, da es ähnlich wie eine Tabelle funktioniert.
Mit `print(response.Text)` kannst du die komplette Antwort sehen. Hier der Code dafür. Teste dies in einer neuen Datei.

```Python
import requests
response = requests.get('<http://official-joke-api.appspot.com/jokes/random>')
print(response.text)
```

Wie du erkennen kannst, ist dies schön formatiert aber noch nicht gut leserlich. So wie es jetzt ist behandelt Python `response.text` noch als String, also als gewöhnlichen Text. Unten siehst du den Datentyp.

![Datentyp](./Bilder/Datentyp.png)

Über die Funktion `json.loads()` kann der String dann in JSON umgewandelt werden. Speichere dies dann in einer Variable ab. Danach kannst du nun die Variable zurückgeben an das Programm.
Kleine Erklärung
Eine Variable ist nur in ihrem Definitionsbereich gültig. Hier etwas code zur Veranschaulichung. Versuche diesen in einer neuen Datei auszuführen. Er dient nur als Beispiel und wird nicht im Programm verwendet.

```Python
x = 5
print("Im normalen Programm ist x = ", x)
def testfunktion():
    x = 10
    print("In der Funktion ist x = ", x)
testfunktion()
print("Und auch danach ist x = ", x)
```

Damit man eine Variable aus einer Funktion erhalten kann, braucht es einen return. Damit wird ein Wert von der Funktion zurückgegeben. Ein Beispiel wäre die `range()` Funktion, welche eine Reihe von Zahlen zurückgibt.

Beispiel mit veränderter Funktion. Teste dies in einer neuen Datei.

```Python
x = 5
print("Im normalen Programm ist x = ", x)
def testfunktion():
    x = 10
    print("In der Funktion ist x = ", x)
    return x
x = testfunktion()
print("Und danach ist x = ", x)
```

Passe nun deine Funktion entsprechend an, sodass «return deine_Variable» am Ende des if teils ist. Wenn du dies getan hast, teste dein Programm aus. Falls ein Fehler auftaucht, lies die Fehlermeldung genau durch und versuche deinen Code zu korrigieren. Frag sonst eine Betreuungskraft nach Hilfe.

### 2.1 Refresh Button

Wenn du den vorherigen Auftrag abgeschlossen hast, kannst du nun versuchen einen Button einzuprogrammieren. Dieser sollte eine neue Abfrage starten und den vorherigen Text ersetzen.
Entferne zuerst die Anführungszeichen bei der Funktion refresh und dort, wo der Button erstellt wird. Der Code muss danach so aussehen. Achte darauf, dass auch alle Anführungszeichen korrekt entfernt wurden.
Danach kannst du nun die refresh Funktion rein programmieren. Überlege dir zuerst, wie genau der Code funktionieren muss und teste ein wenig herum. Falls du stecken bleibst, frag einfach eine Betreuungskraft.

*Kleiner Tipp: Mit `canvas.delete("text")` wird der Text gelöscht.*

### 2.2 Background ändern

Mit dem vorherigen Wissen solltest du nun einen weiteren Button erstellen können, der den Hintergrund ändert. Auch dort musst du beim bestehenden Code die Anführungszeichen entfernen.  Danach musst du ähnlich wie im vorherigen Teil vorgehen. Beachte, dass das Programm jeweils über alles drüber das Bild setzt. Du musst also alles neu erstellen, nachdem der Hintergrund platziert wurde.
