import requests
import json
import tkinter as tk
from PIL import Image, ImageTk

window = tk.Tk()  # Hier wird das Fenster definiert
window.title("Joke's on you")  # Hier wird der Titel gesetzt
window.geometry("500x170")  # Hier wird die Grösse bestimmt

font = ("Segoe UI Emoji", 12)
background_image1 = ImageTk.PhotoImage(Image.open("background1.png"))
background_image2 = ImageTk.PhotoImage(Image.open("background2.png"))
background_image3 = ImageTk.PhotoImage(Image.open("background3.png"))  # Hier wird der Hintergrund geladen
current_background = background_image1

canvas = tk.Canvas(window, width=500, height=170)  # Hier wird ein Bereich zum Zeichnen erstellt
canvas.place(x=-1, y=0)
canvas.create_image(250, 85, image=current_background, tags="background")  # Hier wird der Hintergrund eingefügt


def get_request():
    response = requests.get('http://official-joke-api.appspot.com/jokes/random')
    if response.status_code == 200:  # http Code für erfolgreiche Verbindung
        ################################
        # Hier kommt die Funktion rein #
        ################################
        return json.loads(response.text)
    else:
        print(response.status_code)
        return {}


content = get_request()


def refresh():
    ##################################
    # Refresh Button kommt hier rein #
    ##################################
    global content
    content = get_request()
    canvas.delete("text")
    canvas.create_text(250, 30, text=content['setup'], font=font, tags="text")
    canvas.create_text(250, 60, text=content['punchline'], font=font, tags="text")


refresh = tk.Button(window, text="🔄 Noch einen mehr!", command=refresh, font=font, bg="light blue")
refresh.place(anchor="center", relx=0.5, rely=0.8)


def background_change():
    global current_background
    if current_background == background_image1:
        current_background = background_image2

    elif current_background == background_image2:
        current_background = background_image3

    elif current_background == background_image3:
        current_background = background_image1
    canvas.delete("background")
    canvas.create_image(250, 85, image=current_background, tags="background")
    canvas.create_text(250, 30, text=content['setup'], font=font, tags="text")
    canvas.create_text(250, 60, text=content['punchline'], font=font, tags="text")


background_change = tk.Button(window, text="Background", command=background_change, font=font, bg="grey")
background_change.place(anchor="center", relx=0.89, rely=0.86)

canvas.create_text(250, 30, text=content['setup'], font=font, tags="text")
canvas.create_text(250, 60, text=content['punchline'], font=font, tags="text")


window.mainloop()
