# Excel Formeln

## 1 Aufgabe

In dieser Übung musst du mit Hilfe von Formeln Berechnungen in einer Excel Tabelle durchführen. Die Excel Tabelle ist schon vorgegeben, d.h. du musst die Tabelle nicht neu erstellen. Es handelt sich um eine Notentabelle mit 8 Semesternoten. Du musst diese nun auswerten.
Du findest die vorgegebene Excelvorlage im selben Ordner.

## 2 Vorgehen und Tipps

### 2.1 Formeln anwenden

Um Formeln in Excel anzuwenden, klickst du einfach auf eine Zelle und beginnst mit einem <span style="color: red">`=`</span>. Nun kannst du entweder verschiedene Zellen auswählen und mithilfe der Rechenzeichen verschiedene Berechnungen durchführen. Du kannst aber auch eine bereits vorgegebene Formel verwenden. Willst du zum Beispiel den Durchschnitt der Zellen B5 bis B15, dann gibst du <span style="color: red"> `=MITTELWERT` </span> ein und wählst dann die gewünschten Zellen aus. Du kannst alle Zellen von A1 bis C3 auswählen, indem du den Bereich <span style="color: red"> `A1:C3` </span> angibst. Z.b 
`
=MITTELWERT(A1:C3)
`


### 2.2 Übung

Um die Aufgaben zu lösen, brauchst du nur die folgenden Formeln:

- Mittelwert
- Min
- Max

![Tabelle Beispiel](./Tabelle%20Beispiel.png)
