# Rechner in C\#

## 1 Aufgabenstellung / Dein Ziel

Dein Auftrag ist es, einen Taschenrechner zu erstellen. Er muss möglichst kompakt sein und lediglich die 4 Grundrechenarten (+, -, *, /) beherrschen. Der Taschenrechner soll ebenfalls die Hintergrundfarbe ändern, wenn man eine Rechenoperation ausführt. Schlussendlich sollte der Rechner etwa so aussehen:

![Fertiger Rechner](./Bilder/fertiger_rechner.png)

## 2 Einführung in C\#

Wichtig: Speichere deine Arbeit regelmässig, damit bei einem Absturz möglichst wenig verloren geht.

### 2.1 Was ist C\#?

C\# ist ein Programm, mit dem man kleine Applikationen programmieren kann.
Zuerst kann der Programmierer die Oberfläche des Programms (Das Fenster, Knöpfe, Bilder, Eingabefelder…) erstellen. Von Beginn an hat man ein Formular `Form1`. Darauf zieht man mithilfe der Toolbox die verschiedenen Elemente.
Sobald man die Oberfläche gestaltet hat, kommt das Programmieren an die Reihe. Jedem dieser Elemente kann nun ein Code zugewiesen werden. Beispielsweise kann man nach dem Drücken eines Knopfs das Programm schliessen lassen.

### 2.2 Der Aufbau von C\#

#### 2.2.1 Menüleiste

![Menuleiste](./Bilder/menuleiste.png)

Alle Speichern  Programm ausführen (Play-Icon)
Die Menüleiste dient als Ausgangspunkt aller Aktionen. Die wichtigsten Funktionen sind oben aufgelistet.

#### 2.2.2 Toolbox (Werkzeugleiste)

![Toolbox](./Bilder/Toolbox.png)

In der Werkzeugleiste finden wir unsere benötigten Elemente wie Knöpfe oder Textfelder. Die wichtigsten für unser Projekt sind:

- Knopf (Button)
- Beschriftung (Label)
- Textfeld (TextBox)

Um ein Element auf unser Formular zu bringen, kann man es einfach mit der Maus auf die Oberfläche ziehen oder mithilfe eines Doppelklicks auf das Element.

#### 2.2.3 Eigenschaftenfenster

![Eigenschaften](./Bilder/Properties.png)

Im Eigenschaftenfenster sind, wie der Name schon verrät, die Eigenschaften zu finden. Die Eigenschaften werden immer nur für das aktuell angewählte Element sichtbar. **ACHTUNG**: Auch das Formular ist ein Element! Für uns wichtige Einstellungen sind folgende:

- Text:
  - Das ist der angezeigte Text. Dieser steht entweder auf einem Button oder als Beschriftung beim Label etc. Dieser Text hat nichts mit dem Programmieren zu tun.
- Name:
  - Legt den Namen für das Programm fest. Das Element wird mithilfe dieses Namens im Code angesprochen. Meistens stellt man eine 3-stellige Abkürzung vor das Element. (txt für Textfeld, cmd für Button, lbl für Label, etc.)

Daneben gibt es zahlreiche weitere Eigenschaften wie `visible` (Element anzeigen oder nicht). Visible bedeutet sichtbar und kann mit True (Wahr) und False (nicht Wahr) eingestellt werden.
Tipp:
Die Eigenschaften eines Elements kann man leicht aufrufen. Markiere das Element und drücke F4 und du springst zu den Eigenschaften.

#### 2.2.4 Das Projektfenster

![Projektfenster](./Bilder/Explorer.png)

Im Projektfenster werden alle Formulare sowie alle anderen Programmteile angezeigt.
Bestandteile können beispielsweise Formulare oder Module sein.
Mit diesem Fenster arbeiten wir allerdings nicht.

#### 2.2.5 Ein Formular

![Formular](./Bilder/Forms_entwurf.png)

Das Formular ist eines der Hauptbestandteile deines Programms. Es ist der angezeigte Teil.
Hier kannst du mithilfe der Toolbox Elemente hineinziehen und vergrössern, bzw. verkleinern.
Form1.cs ist der Code für dein Programm, Form1.cs\[Entwurf\] ist die Oberfläche, auf welcher du die verschiedenen Elemente drauf ziehen kannst.

#### 2.2.6 Das Codefenster

![Programm fenster](./Bilder/Code_Fenster.png)

Hier findet das eigentliche Programmieren statt. Mit einem Doppelklick auf ein Element kommt man in dieses Fenster.
Hier kann man nun wählen, was nach welchem Ereignis, mit welchem Element geschieht. Kurz gesagt: du sagst dem Programm, was es tun soll. Um zur Oberfläche zurück zu gelangen, klickst du auf Form1.cs \[Entwurf\].

## 3 Die Entwicklung des Programms

### 3.1 Benutzeroberfläche erstellen

Merken:
Bevor man ein Programm programmiert, erstellt man immer erst die Oberfläche dazu und überlegt sich, was das Programm können muss.
Du startest `Visual Studio 2022` und erstellst dann ein neues Projekt.

![Neues Projekt](./Bilder/Neues_projekt.png)

Nun werden wir gefragt, was für ein Projekt wir erstellen möchten. Wähle `Windows Forms-App .NET Framework`.

![Version](./Bilder/Projekt_version.png)

Nun kannst du dem Projekt noch einen Namen geben. Hier wäre `Taschenrechner` ein möglicher Name. Wähle unter `Ort` die drei Punkte und wähle deinen Ordner `Vorlagen` auf dem Desktop. Du kannst nun auf `Erstellen` klicken.

![Projekt erstellen](./Bilder/Projekt_erstellen.png)

Dein Projekt wurde erstellt. Wähle nun die Toolbox aus. Ist diese nicht vorhanden, kannst du am rechten Rand auf `Toolbox` klicken.

![Entwurf](./Bilder/Entwurf.png)

Erstelle 3 Eingabefelder (TextBox), 4 Knöpfe (Button) und eine Beschriftung (Label). Die Eingabefelder werden zur Eingabe der beiden Operanden und zur Ausgabe des Resultats gebraucht. Mithilfe der 4 Knöpfe sollen später die Operationen ausgeführt werden können und die Beschriftung brauchen wir, um ein Gleich (=) - Zeichen zu setzen. Die Form der Elemente kann immer nachträglich noch beliebig verändert werden.

Nun gehen wir ins Eigenschaftenfenster. Gib nun jedem Element einen passenden Namen. Achte beim Namen auf eine sinnvolle Namensvergabe. Im Beispiel werden die Textfelder `txt_zahl1`, `txt_zahl2` und `txt_resultat` genannt. Die Knöpfe nennen wir `cmd_plus`, `cmd_minus` und so weiter.
Jetzt müssen die Elemente noch eine passende Beschriftung erhalten. Ändere hierzu einfach die Eigenschaft Text. Damit beim Taschenrechner nicht `Form1` im Titel steht, können wir auch hier (bei dem Formular) noch die Eigenschaft Text ändern.
Gut, den ersten Teil des Rechners haben wir geschafft! Mit einem Druck auf das Play-Icon (alternativ auch die F5-Taste) kannst du deinen Rechner begutachten. Allerdings reagiert er noch nicht, da wir noch keinen Code geschrieben haben.
Das Ganze würde dann ungefähr so aussehen:

![Fertiger Rechner](./Bilder/fertiger_rechner.png)

### 3.2 Den Rechner programmieren

Da unser Rechner später auch wirklich rechnen sollen kann, müssen wir noch etwas Code schreiben. Damit der Rechner funktioniert, braucht er allerdings vergleichsweise sehr wenig Code.
Klicke nun doppelt auf den `Plus`-Knopf. Es erscheint ein neues Fenster:

![Programmieren](./Bilder/Button_programmieren.png)

Nun können wir programmieren. Jeglichen Code schreibt man immer zwischen `private void` und der schliessenden Klammer. Hier gibst du nun folgendes ein, wenn du die TextBoxen umbenannt hast:
  
```C#
    int Resultat = 0;
    int Zahl1 = Convert.ToInt16(txt_zahl1.Text);
    int Zahl2 = Convert.ToInt16(txt_zahl2.Text);

    Resultat = Zahl1 + Zahl2;

    txt_resultat.Text = Convert.ToString(Resultat);
```

In Sprache übersetzt heiss dies:
*Der Text im Feld txt_resultat = der numerische Wert des Textes im ersten Feld + der numerische Wert des Textes im zweiten Feld.*

**Achtung!** Wenn du die TextBoxen nicht wie in der Anleitung umbenannt hast, heissen sie standardmässig `textBox1`, `textBox2`, usw... In diesem Fall musst du folgendes eingeben:

```C#
int Resultat = 0;
        int Zahl1 = Convert.ToInt16(textBox1.Text);
        int Zahl2 = Convert.ToInt16(textBox2.Text);

             Resultat = Zahl1 + Zahl2;

    textBox3.Text = Convert.ToString(Resultat);
```

Die `Convert.ToInt16()` Funktion gibt einen Zahlenwert (numerischer Wert) zurück. Wenn du das, was du vorhin programmiert hast, nicht ganz verstehst, frage deine Aufsichtsperson um Hilfe. Nun beherrscht dein Taschenrechner aber erst eine Funktion, nämlich die Addition. Führe nun selbstständig die weiteren 3 Operationen durch. Während der Arbeit solltest du deinen Taschenrechner jeweils speichern. Hierzu klickst du auf den Speichern-Button in der Menüleiste oder drückst CTRL + S auf deiner Tastatur. Zwischendurch kannst du den Rechner ausführen und nachschauen, ob er funktioniert. Immer, wenn etwas im Code unterstrichen ist, kannst du davon ausgehen, dass etwas nicht stimmt.

Damit dein Rechner nun noch etwas Farbe bekommt, fügst du unter den von dir geschriebenen Zeilen noch ein.
`this.BackColor = Color.Blue;`
Experimentiere mit den Farben und den Vorschlägen, die das Programm dir macht!

## 4 Die EXE-Datei

Damit du den Rechner auch ohne Visual Studio ausführen kannst, brauchst du noch die EXE-Datei, die beim Programmieren selbstständig erstellt wurde. Du findest sie in deinem Projektordner unter `bin` -> `Debug`. Versuche diese EXE-Datei zu finden.
Diese EXE-Datei läuft von ganz alleine, du kannst sie also von deinem PC aus starten. Die EXE-Datei kann nicht mehr geändert werden!

## 5 Erweiterungsvorschläge

Wenn du genügend Zeit hast, kannst du noch einige Funktionen hinzufügen. Falls du hier Fragen hast, unterstützt dich deine Aufsichtsperson gerne.

- Füge einen `Zurücksetzen`-Knopf hinzu, der dir alle Textfelder leert.

- Erstelle einen Button mit dem du den Hintergrund zu einer zufälligen Farbe änderst.
