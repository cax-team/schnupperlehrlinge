# Schnupperlehrlinge

## Übersicht

Hier sind die Dateien zum Schnuppertag abgelegt. Es befinden sich die Aufträge in ihrem eigenen Ordner mit allen entsprechenden Vorlagen.
Details zu den spezifischen Dateien in den Ordnern können in den README.md gefunden werden.

## Benutzung

Das Repository sollte geklont werden auf den Schnupper PC mit `git clone git@gitlab.com:cax-team/schnupperlehrlinge.git` oder `git clone https://gitlab.com/cax-team/schnupperlehrlinge.git`, falls es nicht vorhanden ist.
Um das Repository nach dem Tag zu bereinigen sollte `git clean -fd` verwendet werden um alle Daten die möglicherweise neu erstellt wurden zu entfernen.
Danach kann mit `git pull` und `git reset --hard HEAD` das Repository zurückgesetzt werden.

## [Aufträge](./Auftraege/Readme.md)
